﻿namespace App.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddParentsName : DbMigration
    {
        public override void Up()
        {
            AddColumn("public.AspNetUsers", "ParentsName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("public.AspNetUsers", "ParentsName");
        }
    }
}
